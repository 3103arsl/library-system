<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

    <!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/login-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Sep 2017 06:00:08 GMT -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>Library System</title>
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome/css/font-awesome.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN STACK CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <!-- END STACK CSS-->

    </head>
    <body data-open="hover" data-menu="horizontal-menu" data-col="1-column" class="horizontal-layout horizontal-menu 1-column   menu-expanded blank-page blank-page">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
        <div class="app-content content container-fluid">
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body"><section class="flexbox-container">
                        <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1  box-shadow-2 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header no-border">

                                    <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Library System</span></h6>
                                </div>
                                <div class="card-body collapse in">
                                    <div class="card-block">

                                        <?php
                                        if (!empty($success_msg)) {
                                            echo '<p class="statusMsg">' . $success_msg . '</p>';
                                        } elseif (!empty($error_msg)) {
                                            echo '<p class="statusMsg">' . $error_msg . '</p>';
                                        }
                                        ?>

                                        <form class="form-horizontal form-simple" action="" method="post" novalidate>
                                            <fieldset class="form-group position-relative has-icon-left mb-0">
                                                <input type="email" class="form-control form-control-lg input-lg" required="" id="user-name" name="email" placeholder="Email" required>
                                                <?php echo form_error('email', '<span class="help-block">', '</span>'); ?>
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control form-control-lg input-lg" required="" id="user-password" name="password" placeholder="Enter Password" required>
                                                <?php echo form_error('password', '<span class="help-block">', '</span>'); ?>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>

                                            <input type="submit" name="loginSubmit" class="btn btn-primary btn-lg btn-block" value="Submit"/>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </section>

                </div>
            </div>
        </div>
        <!-- ////////////////////////////////////////////////////////////////////////////-->


    </div>

</body>

<!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/login-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Sep 2017 06:00:15 GMT -->
</html>
