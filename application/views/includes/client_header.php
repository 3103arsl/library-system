<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

    <!-- Mirrored from pixinvent.com/stack-responsive-bootstrap-4-admin-template/html/ltr/horizontal-menu-template-nav/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 18 Sep 2017 05:55:07 GMT -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="PIXINVENT">
        <title>Library System</title>
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/images/ico/apple-icon-120.png">
        <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/font-awesome/css/font-awesome.min.css">
        <!-- END VENDOR CSS-->
        <!-- BEGIN STACK CSS-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-extended.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/colors.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <!-- END STACK CSS-->

    </head>
    <body data-open="hover" data-menu="horizontal-menu" data-col="2-columns" class="horizontal-layout horizontal-menu 2-columns   menu-expanded">

        <nav class="header-navbar navbar navbar-with-menu navbar-static-top navbar-dark bg-gradient-x-grey-blue navbar-border navbar-brand-center">
            <div class="navbar-wrapper">
                <div class="navbar-header">
                    <ul class="nav navbar-nav">
                        <li class="nav-item"><a href="<?php echo base_url('dashboard'); ?>" class="navbar-brand"><img alt="stack admin logo" src="<?php echo base_url(); ?>assets/images/logo/stack-logo-light.png" class="brand-logo">
                            </a></li>
                        <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
                    </ul>
                </div>
                <div class="navbar-container content container-fluid">
                    <div id="navbar-mobile" class="collapse navbar-toggleable-sm">

                        <ul class="nav navbar-nav float-xs-right">
                            <li class="dropdown dropdown-user nav-item"><a href="javascript:void(0);" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                                    <span class="user-name"><?php echo $this->session->userdata("userName") ?> </span></a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <div class="dropdown-divider"></div><a href="<?php echo base_url('/logout'); ?>" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>


        <!-- Horizontal navigation-->
        <div role="navigation" data-menu="menu-wrapper" class="header-navbar navbar navbar-horizontal navbar-fixed navbar-light navbar-without-dd-arrow navbar-shadow menu-border">
            <!-- Horizontal menu content-->
            <div data-menu="menu-container" class="navbar-container main-menu-content">
                <!-- include ../../../includes/mixins-->
                <ul id="main-menu-navigation" data-menu="menu-navigation" class="nav navbar-nav">
                    
                     <li data-menu="dropdown" class="nav-item"><a href="<?php echo base_url('client/racks'); ?>" class="nav-link"><i class="ft-bar-chart-2"></i><span>Racks</span></a>
                        </li>
                     <li data-menu="dropdown" class="nav-item"><a href="<?php echo base_url('client/racks/search'); ?>" class="nav-link"><i class="ft-bar-chart-2"></i><span>Search Book</span></a>
                        </li>
                </ul>
            </div>
            <!-- /horizontal menu content-->
        </div>
        <!-- Horizontal navigation-->


      