<?php if (!empty($success_msg)) { ?>
    <div class = "alert bg-success alert-dismissible fade in mb-2" role = "alert">
        <button type = "button" class = "close" data-dismiss = "alert" aria-label = "Close">
            <span aria-hidden = "true">×</span>
        </button>
        <strong>Success!</strong> <?php echo $success_msg ?>
    </div>
    <?php } elseif (!empty($error_msg)) {
    ?>
    <div class="alert bg-danger alert-dismissible fade in mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <strong>Error!</strong> <?php echo $error_msg ?>
    </div>
<?php } ?>

