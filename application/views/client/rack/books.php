
<div class="app-content content container-fluid">
    <div class="content-wrapper">

        <div class="content-body"><!-- Basic Tables start -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title">All Books</h1>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard">
                                <?php $this->load->view('includes/messages'); ?>

                                <?php if ($isSearch): ?>
                                    <form action="" method="get">
                                        <div class="form-group col-md-9">
                                            <input type="text" class="form-control" name="keyword" placeholder="Search book by name, author, published year" required="" value="<?php echo $keyword; ?>">
                                            <?php echo form_error('name', '<span class="help-block">', '</span>'); ?>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <input type="submit" name="bookSearchSubmit" class="btn mr-1 mb-1 btn-primary" value="Submit"/>
                                        </div>
                                    </form>
                                <?php endif; ?>

                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                        <th>Name</th>
                                        <th>Author</th>
                                        <th>Published year</th>
                                        <th>Rack Name</th>
                                        <th>Created</th>
                                        </thead>
                                        <tbody>
                                            <?php if (!empty($results)): ?>
                                                <?php foreach ($results as $data) { ?>
                                                    <tr>
                                                        <td><?php echo $data->name ?></td>
                                                        <td><?php echo $data->author ?></td>
                                                        <td><?php echo $data->published_year ?></td>
                                                        <td><?php echo $data->rack_name ?></td>
                                                        <td><?php echo ($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : 'N/A' ?></td>

                                                    </tr>
                                                <?php } endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-xl-12 col-lg-12">
                                    <div class="text-xs-center mb-3">
                                        <nav aria-label="Page navigation">
                                            <?php
                                            echo ($links != '') ? $links : '';
                                            ?>
                                            <!--                                            <ul class="pagination">
                                                                                            <li class="page-item"></li>
                                                                                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                                                            <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                                                                            <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                                                            <li class="page-item"><a class="page-link" href="#">5</a></li>
                                                                                            <li class="page-item"></li>
                                                                                        </ul>-->
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Tables end -->
        </div>
    </div>
</div>