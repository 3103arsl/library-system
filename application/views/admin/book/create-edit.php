<div class="app-content content container-fluid">
    <div class="content-wrapper">

        <div class="content-body"><!-- Basic Tables start -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title"><?php echo ($isEdit) ? 'Edit' : 'Create'; ?> Book</h1>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard">
                                <?php $this->load->view('includes/messages'); ?>
                                <form action="" method="post">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" placeholder="Name" required="" value="<?php echo!empty($model['name']) ? $model['name'] : ''; ?>">
                                        <?php echo form_error('name', '<span class="help-block">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="author" placeholder="Author" required="" value="<?php echo!empty($model['author']) ? $model['author'] : ''; ?>">
                                        <?php echo form_error('author', '<span class="help-block">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="published_year" placeholder="Published Year" required="" value="<?php echo!empty($model['published_year']) ? $model['published_year'] : ''; ?>">
                                        <?php echo form_error('published_year', '<span class="help-block">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <fieldset class="form-group">
                                            <label for="basicSelect">Basic Select</label>
                                            <select name="rack_id" class="form-control" id="basicSelect">
                                                <?php if (!empty($racks)): ?>
                                                    <?php
                                                    foreach ($racks as $rack):
                                                        $selected = '';
                                                        if ($isEdit && $model['rack_id']==$rack['id']) {
                                                            $selected = 'selected=selected';
                                                        }
                                                        ?>
                                                        <option <?php echo $selected; ?> value="<?php echo $rack['id']; ?>"><?php echo $rack['name']; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </fieldset>
                                        <?php echo form_error('published_year', '<span class="help-block">', '</span>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php
                                        if (!$isEdit) {
                                            if (!empty($model['status']) && $model['status'] == STATUS_INACTIVE) {
                                                $inActiveCheck = 'checked="checked"';
                                                $activeCheck = '';
                                            } else {
                                                $activeCheck = 'checked="checked"';
                                                $inActiveCheck = '';
                                            }
                                        } else {
                                            if ($model['status'] == STATUS_INACTIVE) {
                                                $inActiveCheck = 'checked="checked"';
                                                $activeCheck = '';
                                            } else {
                                                $activeCheck = 'checked="checked"';
                                                $inActiveCheck = '';
                                            }
                                        }
                                        ?>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="status" value="1" <?php echo $activeCheck; ?>>
                                                Active
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="status" value="0" <?php echo $inActiveCheck; ?>>
                                                InActive
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="submit" name="bookSubmit" class="btn mr-1 mb-1 btn-primary" value="Submit"/>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Tables end -->
        </div>
    </div>
</div>