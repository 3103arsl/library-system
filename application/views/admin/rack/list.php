
<div class="app-content content container-fluid">
    <div class="content-wrapper">

        <div class="content-body"><!-- Basic Tables start -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="card">
                        <div class="card-header">
                            <h1 class="card-title">All Racks</h1>
                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block card-dashboard">
                                 <?php $this->load->view('includes/messages'); ?>
                                <div class="table-responsive">
                                    <table class="table mb-0">
                                        <thead>
                                        <th>Name</th>
                                        <th>Status</th>
                                        <th>Total Books</th>
                                        <th>Created</th>
                                        <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <?php if(!empty($results)): ?>
                                            <?php foreach ($results as $data) { ?>
                                                <tr>
                                                    <td><?php echo $data->name ?></td>
                                                    <td><?php echo ($data->status == STATUS_ACTIVE) ? 'Publish' : 'Draft' ?></td>
                                                    <td><?php echo $data->total_books ?></td>
                                                    <td><?php echo ($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : 'N/A' ?></td>
                                                    <td>

                                                        <a class="btn mr-1 mb-1 btn-primary" style="float: left" href="<?php echo base_url('admin/racks/edit/' . $data->id); ?>">Edit</a>
                                                        <form style="float: left;"  method="post" action="<?php echo base_url('admin/racks/delete'); ?>">
                                                            <input type="hidden" name="id" value="<?php echo $data->id ?>">
                                                            <input class="btn btn-icon btn-danger" onclick="return confirm('Want to delete?');" type="submit" name="rackSubmit" class="btn-primary" value="Delete"/>
                                                        </form>

                                                    </td>
                                                </tr>
                                            <?php } endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <div class="row">

                                <div class="col-xl-12 col-lg-12">
                                    <div class="text-xs-center mb-3">
                                        <nav aria-label="Page navigation">
                                          <?php
                                            echo ($links != '') ? $links : '';
                                            ?>
<!--                                            <ul class="pagination">
                                                <li class="page-item"></li>
                                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item active"><a class="page-link" href="#">3</a></li>
                                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                                <li class="page-item"></li>
                                            </ul>-->
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Basic Tables end -->
        </div>
    </div>
</div>