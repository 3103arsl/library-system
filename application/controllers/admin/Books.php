<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Books Management
 */
class Books extends CI_Controller {

    private $rackBookLimit = 10;

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('book');
        $this->load->model('rack');

        if (!$this->session->userdata('isAdminUserLoggedIn')) { // Redirect if not logged in
            redirect('admin/users/login');
        }
    }

    /*
     * Racks list
     */

    public function index() {

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $params = array(); // init params
        $limit_per_page = 5;
        $total_records = $this->book->getTotal();
        $params["links"] = '';
        if ($total_records > 0) {
            $config['base_url'] = base_url() . 'admin/books';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'class="page-link"';
            $start_index = ($this->input->get('page')) ? ( ( $this->input->get('page') - 1 ) * $config["per_page"] ) : 0;
            $params["results"] = $this->book->getCurrentPageRecords($limit_per_page, $start_index); // get current page records
            $this->pagination->initialize($config);
            $params["links"] = $this->pagination->create_links(); // build paging links
        }
        $this->load->view('includes/header', $params);
        $this->load->view('admin/book/list', $params);
        $this->load->view('includes/footer', $params);
    }

    /*
     * Rack Create
     */

    public function create() {
        $data = array();
        $model = array();
        if ($this->input->post('bookSubmit')) {
            $this->form_validation->set_rules('name', 'Name', 'required|callback_book_check');
            $this->form_validation->set_rules('author', 'Author', 'required');
            $this->form_validation->set_rules('published_year', 'Published Year', 'required');
            $this->form_validation->set_rules('rack_id', 'Rack ID', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $model = array(
                'name' => strip_tags($this->input->post('name')),
                'author' => strip_tags($this->input->post('author')),
                'published_year' => strip_tags($this->input->post('published_year')),
                'rack_id' => strip_tags($this->input->post('rack_id')),
                'status' => strip_tags($this->input->post('status')),
            );

            if ($this->form_validation->run() == true) {
                $totalBooks = $this->book->getRackBookCount(strip_tags($this->input->post('rack_id')));

                if ($totalBooks > 10) {
                    $this->session->set_userdata('error_msg', 'Insufficient space in rack please choose other one.');
                    redirect('admin/books/create');
                }

                $insert = $this->book->insert($model);
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Book was created successfully.');
                    redirect('admin/books/create');
                } else {
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['model'] = $model;
        $data['isEdit'] = false;
        $data['racks'] = $this->rack->getRows();

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        //load the view
        $this->load->view('includes/header', $data);
        $this->load->view('admin/book/create-edit', $data);
        $this->load->view('includes/footer', $data);
    }

    /*
     * Rack Edit
     */

    public function edit($id) {
        $data = array();
        $rack = $this->book->getRows(['returnType' => 'single', 'conditions' => array('id' => $id)]);
        if (empty($rack)) {
            redirect('admin/books');
        }
        $model = array();
        if ($this->input->post('bookSubmit')) {
            if ($rack['name'] != strip_tags($this->input->post('name'))) {
                $this->form_validation->set_rules('name', 'Name', 'required|callback_book_check');
            }

            $this->form_validation->set_rules('author', 'Author', 'required');
            $this->form_validation->set_rules('published_year', 'Published Year', 'required');
            $this->form_validation->set_rules('rack_id', 'Rack ID', 'required');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $model = array(
                'name' => strip_tags($this->input->post('name')),
                'author' => strip_tags($this->input->post('author')),
                'published_year' => strip_tags($this->input->post('published_year')),
                'rack_id' => strip_tags($this->input->post('rack_id')),
                'status' => strip_tags($this->input->post('status')),
            );

            if ($this->form_validation->run() == true) {
                $insert = $this->book->update($id, $model);
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Book was update successfully.');
                    redirect('admin/books/edit/' . $id);
                } else {
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['model'] = $rack;
        $data['isEdit'] = true;
        $data['racks'] = $this->rack->getRows();

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        //load the view
        $this->load->view('includes/header', $data);
        $this->load->view('admin/book/create-edit', $data);
        $this->load->view('includes/footer', $data);
    }

    /*
     * Rack Delete
     */

    public function delete() {
        if ($this->input->post('rackSubmit')) {
            $this->form_validation->set_rules('id', 'ID', 'required');
            if ($this->form_validation->run() == true) {
                $insert = $this->rack->delete(strip_tags($this->input->post('id')));
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Rack was deleted successfully.');
                } else {
                    $this->session->set_userdata('error_msg', 'Some problems occured, please try again.');
                }
            }
        }
        redirect('admin/racks');
    }

    /*
     * Existing email check during validation
     */

    public function book_check($str) {
        $con['returnType'] = 'count';
        $con['conditions'] = array('name' => $str);
        $checkEmail = $this->rack->getRows($con);
        if ($checkEmail > 0) {
            $this->form_validation->set_message('rack_check', 'The given rack already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
