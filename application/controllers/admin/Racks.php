<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Racks Management
 */
class Racks extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('rack');

        if (!$this->session->userdata('isAdminUserLoggedIn')) { // Redirect if not logged in
            redirect('admin/users/login');
        }
    }

    /*
     * Racks list
     */

    public function index() {

  
        
        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $params = array(); // init params
        $limit_per_page = 5;
        $total_records = $this->rack->getTotal();
        $params["links"] = '';

        if ($total_records > 0) {
            $config['base_url'] = base_url() . 'admin/racks';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'class="page-link"';
            $start_index = ($this->input->get('page')) ? ( ( $this->input->get('page') - 1 ) * $config["per_page"] ) : 0;
            $params["results"] = $this->rack->getCurrentPageRecords($limit_per_page, $start_index); // get current page records
            $this->pagination->initialize($config);
            $params["links"] = $this->pagination->create_links(); // build paging links
        }
        $this->load->view('includes/header', $params);
        $this->load->view('admin/rack/list', $params);
        $this->load->view('includes/footer', $params);
    }

    /*
     * Rack Create
     */

    public function create() {
        $data = array();
        $model = array();
        if ($this->input->post('rackSubmit')) {
            $this->form_validation->set_rules('name', 'Name', 'required|callback_rack_check');
            $this->form_validation->set_rules('status', 'Status', 'required');
            $model = array(
                'name' => strip_tags($this->input->post('name')),
                'status' => strip_tags($this->input->post('status')),
            );

            if ($this->form_validation->run() == true) {
                $insert = $this->rack->insert($model);
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Rack was created successfully.');
                    redirect('admin/racks/create');
                } else {
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['model'] = $model;
        $data['isEdit'] = false;

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        //load the view
        $this->load->view('includes/header', $data);
        $this->load->view('admin/rack/create-edit', $data);
        $this->load->view('includes/footer', $data);
    }

    /*
     * Rack Edit
     */

    public function edit($id) {
        $data = array();
        $rack = $this->rack->getRows(['returnType' => 'single', 'conditions' => array('id' => $id)]);
        if (empty($rack)) {
            redirect('admin/racks');
        }
        $model = array();
        if ($this->input->post('rackSubmit')) {
            if ($rack['name'] != strip_tags($this->input->post('name'))) {
                $this->form_validation->set_rules('name', 'Name', 'required|callback_rack_check');
            }
            $this->form_validation->set_rules('status', 'Status', 'required');
            $model = array(
                'name' => strip_tags($this->input->post('name')),
                'status' => strip_tags($this->input->post('status')),
            );

            if ($this->form_validation->run() == true) {
                $insert = $this->rack->update($id, $model);
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Rack was update successfully.');
                    redirect('admin/racks/edit/' . $id);
                } else {
                    $data['error_msg'] = 'Some problems occured, please try again.';
                }
            }
        }
        $data['model'] = $rack;
        $data['isEdit'] = true;

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        //load the view
        $this->load->view('includes/header', $data);
        $this->load->view('admin/rack/create-edit', $data);
        $this->load->view('includes/footer', $data);
    }

    /*
     * Rack Delete
     */

    public function delete() {
        if ($this->input->post('rackSubmit')) {
            $this->form_validation->set_rules('id', 'ID', 'required');
            if ($this->form_validation->run() == true) {
                $insert = $this->rack->delete(strip_tags($this->input->post('id')));
                if ($insert) {
                    $this->session->set_userdata('success_msg', 'Rack was deleted successfully.');
                } else {
                    $this->session->set_userdata('error_msg', 'Some problems occured, please try again.');
                }
            }
        }
        redirect('admin/racks');
    }

    /*
     * Existing email check during validation
     */

    public function rack_check($str) {
        $con['returnType'] = 'count';
        $con['conditions'] = array('name' => $str);
        $checkEmail = $this->rack->getRows($con);
        if ($checkEmail > 0) {
            $this->form_validation->set_message('rack_check', 'The given rack already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
