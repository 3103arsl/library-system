<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Racks Management
 */
class Racks extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('pagination');
        $this->load->model('rack');
        $this->load->model('book');

        if (!$this->session->userdata('isUserLoggedIn')) { // Redirect if not logged in
            redirect('/');
        }
    }

    /*
     * Racks list
     */

    public function index() {

        if ($this->session->userdata('success_msg')) {
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if ($this->session->userdata('error_msg')) {
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        $params = array(); // init params
        $limit_per_page = 5;
        $total_records = $this->rack->getTotal();
        $params["links"] = '';

        if ($total_records > 0) {
            $config['base_url'] = base_url() . 'admin/racks';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'page';
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';
            $config['num_tag_open'] = '<li class="page-item">';
            $config['num_tag_close'] = '</li>';
            $config['anchor_class'] = 'class="page-link"';
            $start_index = ($this->input->get('page')) ? ( ( $this->input->get('page') - 1 ) * $config["per_page"] ) : 0;
            $params["results"] = $this->rack->getCurrentPageRecords($limit_per_page, $start_index); // get current page records
            $this->pagination->initialize($config);
            $params["links"] = $this->pagination->create_links(); // build paging links
        }
        $this->load->view('includes/client_header', $params);
        $this->load->view('client/rack/racks', $params);
        $this->load->view('includes/footer', $params);
    }

    public function books($id) {
        $params["isSearch"] = false;
        $params["links"] = '';
        $params["keyword"] = '';
        $params['results'] = $this->book->getBooksByRack($id);

        $this->load->view('includes/client_header', $params);
        $this->load->view('client/rack/books', $params);
        $this->load->view('includes/footer', $params);
    }

    public function search() {
        $params["isSearch"] = TRUE;
        $params["links"] = '';
        $params['results'] = [];
        $params["keyword"] = '';

        if ($this->input->get('bookSearchSubmit')) {
            $keyword = strip_tags($this->input->get('keyword'));
            $params['results'] = $this->book->getSearchedBooks($keyword);
            $params["keyword"] = $keyword;
        }

        $this->load->view('includes/client_header', $params);
        $this->load->view('client/rack/books', $params);
        $this->load->view('includes/footer', $params);
    }

}
