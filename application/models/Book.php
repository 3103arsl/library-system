<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Book extends CI_Model {

    function __construct() {
        $this->userTbl = 'books';
    }

    /*
     * get rows from the racks table
     */

    function getRows($params = array()) {
        $this->db->select('*');
        $this->db->from($this->userTbl);


        if (array_key_exists("conditions", $params)) { //fetch data by conditions
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if (array_key_exists("id", $params)) {
            $this->db->where('id', $params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {

            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) { //set start and limit
                $this->db->limit($params['limit'], $params['start']);
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if (array_key_exists("returnType", $params) && $params['returnType'] == 'count') {
                $result = $query->num_rows();
            } elseif (array_key_exists("returnType", $params) && $params['returnType'] == 'single') {
                $result = ($query->num_rows() > 0) ? $query->row_array() : FALSE;
            } else {
                $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
            }
        }

        return $result; //return fetched data
    }

    /*
     * Insert user information
     */

    public function insert($data = array()) {

        if (!array_key_exists("created", $data)) { //add created and modified data if not included
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if (!array_key_exists("modified", $data)) {
            $data['updated_at'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert($this->userTbl, $data); //insert user data to users table
        if ($insert) { //return the status
            return $this->db->insert_id();
            ;
        } else {
            return false;
        }
    }

    public function update($id, $data) { // Update Row
        $this->db->where('id', $id);
        $this->db->update($this->userTbl, $data);
        return true;
    }

    public function delete($id) { // Delete Row
        $this->db->delete($this->userTbl, ['id' => $id]);
        return true;
    }

    public function getTotal() { // Get Total Row
        return $this->db->count_all($this->userTbl);
    }

    public function getCurrentPageRecords($limit, $start) { // Get Rows by limit
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->userTbl);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function getRackBookCount($rackId) {
        //$this->db->select('count(id) as total');
        $this->db->from($this->userTbl);
        $this->db->where('rack_id', $rackId);
        $query = $this->db->get();
        return $query->num_rows();

        //return $this->db->query('SELECT COUNT(id) AS total FROM `books` WHERE rack_id = 12');
    }

    public function getBooksByRack($rackId) {
        $this->db->select('books.*,racks.name as rack_name');
        $this->db->from($this->userTbl);
        $this->db->join('racks', 'books.rack_id=racks.id');
        $this->db->where('rack_id', $rackId);
        $query = $this->db->get();
        return $query->result();
    }

    public function getSearchedBooks($keyword) {
        //$keyword = '%' . $keyword . '%';
        $this->db->select('books.*,racks.name as rack_name');
        $this->db->from($this->userTbl);
        $this->db->join('racks', 'books.rack_id=racks.id');
        $this->db->like('books.name', $keyword);
        $this->db->or_like('books.author', $keyword);
        $this->db->or_like('books.published_year', $keyword);
        $this->db->or_like('books.name', ucfirst($keyword));
        $this->db->or_like('books.author', ucfirst($keyword));
        $this->db->or_like('books.published_year', ucfirst($keyword));
        $query = $this->db->get();
        return $query->result();
    }

}
