<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rack extends CI_Model {

    function __construct() {
        $this->userTbl = 'racks';
    }

    /*
     * get rows from the racks table
     */

    function getRows($params = array()) {
        $this->db->select('*');
        $this->db->from($this->userTbl);


        if (array_key_exists("conditions", $params)) { //fetch data by conditions
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if (array_key_exists("id", $params)) {
            $this->db->where('id', $params['id']);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {

            if (array_key_exists("start", $params) && array_key_exists("limit", $params)) { //set start and limit
                $this->db->limit($params['limit'], $params['start']);
            } elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params)) {
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if (array_key_exists("returnType", $params) && $params['returnType'] == 'count') {
                $result = $query->num_rows();
            } elseif (array_key_exists("returnType", $params) && $params['returnType'] == 'single') {
                $result = ($query->num_rows() > 0) ? $query->row_array() : FALSE;
            } else {
                $result = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
            }
        }

        return $result; //return fetched data
    }

    /*
     * Insert user information
     */

    public function insert($data = array()) {

        if (!array_key_exists("created", $data)) { //add created and modified data if not included
            $data['created_at'] = date("Y-m-d H:i:s");
        }
        if (!array_key_exists("modified", $data)) {
            $data['updated_at'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert($this->userTbl, $data); //insert user data to users table
        if ($insert) { //return the status
            return $this->db->insert_id();
            ;
        } else {
            return false;
        }
    }

    public function update($id, $data) { // Update Row
        $this->db->where('id', $id);
        $this->db->update($this->userTbl, $data);
        return true;
    }

    public function delete($id) { // Delete Row
        $this->db->delete($this->userTbl, ['id' => $id]);
        return true;
    }

    public function getTotal() { // Get Total Row
        return $this->db->count_all($this->userTbl);
    }

    public function getCurrentPageRecords($limit, $start) { // Get Rows by limit
        $this->db->limit($limit, $start);
        $this->db->select('racks.*,count(books.id) as total_books');
        $this->db->from($this->userTbl);
        $this->db->join('books', 'books.rack_id = racks.id', 'left');
        $this->db->group_by('racks.id'); 
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }
    

//    public function test() {
//        $this->db->select('racks.*,count(books.id) as total_books');
//        $this->db->from($this->userTbl);
//        $this->db->join('books', 'books.rack_id = racks.id', 'left');
//        $this->db->group_by('racks.id'); 
//        $query = $this->db->get();
//       return $query->result();
//    }

}
