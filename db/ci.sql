-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2018 at 08:06 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `rack_id` int(11) DEFAULT NULL,
  `name` varbinary(50) DEFAULT NULL,
  `author` varbinary(50) DEFAULT NULL,
  `published_year` varbinary(15) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `rack_id`, `name`, `author`, `published_year`, `status`, `created_at`, `updated_at`) VALUES
(1, 12, 0x426f6f6b, 0x4f78666f7264, 0x32303138, 1, '2018-03-01 09:50:03', '2018-03-01 09:50:03'),
(2, 12, 0x426f6f6b2031, 0x50656c6772617665, 0x32303138, 1, '2018-03-02 06:46:10', '2018-03-01 09:50:15'),
(3, 12, 0x426f6f6b2032, 0x4f78666f7264, 0x32303138, 1, '2018-03-01 09:50:46', '2018-03-01 09:50:46'),
(4, 12, 0x426f6f6b2033, 0x4f78666f7264, 0x32303138, 1, '2018-03-01 09:50:54', '2018-03-01 09:50:54'),
(5, 12, 0x426f6f6b2034, 0x506172616d6f756e74, 0x32303136, 1, '2018-03-02 06:47:13', '2018-03-01 09:51:05'),
(6, 12, 0x426f6f6b2035, 0x4f78666f7264, 0x32303136, 1, '2018-03-02 06:47:06', '2018-03-01 09:51:18'),
(7, 12, 0x426f6f6b2036, 0x4f78666f7264, 0x32303138, 1, '2018-03-01 09:51:29', '2018-03-01 09:51:29'),
(8, 12, 0x426f6f6b2037, 0x50656c6772617665, 0x32303135, 1, '2018-03-02 06:47:16', '2018-03-01 09:51:40'),
(9, 12, 0x426f6f6b2038, 0x4f78666f7264, 0x32303137, 1, '2018-03-02 06:47:03', '2018-03-01 09:51:51'),
(10, 15, 0x426f6f6b2039, 0x506172616d6f756e74, 0x32303138, 1, '2018-03-02 06:45:38', '2018-03-01 09:51:59'),
(11, 13, 0x426f6f6b203130, 0x4f78666f7264, 0x32303138, 1, '2018-03-01 13:58:10', '2018-03-01 09:52:09'),
(12, 14, 0x426f6f6b203131, 0x50656c6772617665, 0x32303137, 1, '2018-03-02 06:47:09', '2018-03-01 09:53:48'),
(13, 13, 0x426f6f6b73, 0x506172616d6f756e74, 0x32303138, 1, '2018-03-02 06:45:39', '2018-03-01 09:54:08'),
(14, 13, 0x426f6f6b732032, 0x4f78666f7264, 0x32303137, 1, '2018-03-02 06:47:11', '2018-03-01 09:57:34'),
(15, 12, 0x4120486973746f7279206f66204d61727869616e2045636f6e6f6d696373202850617065726261636b29, 0x50616c6772617665, 0x32303134, 1, '2018-03-02 02:58:15', '2018-03-02 02:58:15');

-- --------------------------------------------------------

--
-- Table structure for table `racks`
--

CREATE TABLE `racks` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `racks`
--

INSERT INTO `racks` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(12, 'Rack 1', 1, '2018-03-01 09:37:30', '2018-03-01 09:37:30'),
(13, 'Rack 2', 1, '2018-03-01 09:37:35', '2018-03-01 09:37:35'),
(14, 'Rack 3', 1, '2018-03-01 09:37:41', '2018-03-01 09:37:41'),
(15, 'Rack 4', 1, '2018-03-01 09:37:45', '2018-03-01 09:37:45'),
(16, 'Rack 15', 1, '2018-03-02 02:58:50', '2018-03-02 02:58:50');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `status`, `created_at`, `updated_at`) VALUES
(2, 'admin', 'admin@gm.com', 'fd57fd1ca2cc147f5191f33c64af40e6', 1, 1, '2018-03-01 14:19:12', '2018-03-01 03:18:03'),
(3, 'Arslan', '3103arsl@gm.com', 'fd57fd1ca2cc147f5191f33c64af40e6', 0, 1, '2018-03-01 14:19:35', '2018-03-01 03:18:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `books_ibfk_1` (`rack_id`);

--
-- Indexes for table `racks`
--
ALTER TABLE `racks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `racks`
--
ALTER TABLE `racks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`rack_id`) REFERENCES `racks` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
